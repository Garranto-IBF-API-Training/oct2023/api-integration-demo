package com.garranto;

import com.google.gson.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.Scanner;

public class CountrySearchApp {

    public static void main(String[] args) {
        System.out.println("Country Search App");
        System.out.println("=====================");

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Gson gson = new Gson();

        String code;

        System.out.println("Enter a valid currency code to continue");
        Scanner scanner = new Scanner(System.in);
        code = scanner.next();

        String url = "https://restcountries.com/v3.1/currency/"+code;

//        request that needs to be send
        Request request = new Request.Builder()
                .url(url)
                .method("GET",null)
                .build();

        try {
            Response response = client.newCall(request).execute();

            String countriesJSONArrayString = response.body().string();


//            array of elements
//            array of objects or array primitives or array of arrays

            JsonArray countriesJSONArray = gson.fromJson(countriesJSONArrayString,JsonArray.class);

            System.out.println(countriesJSONArray.size());

            for(JsonElement countryElement:countriesJSONArray){

//                JsonObject country = countryElement.getAsJsonObject();
//                JsonObject nameObject = country.getAsJsonObject("name");
//                JsonPrimitive officialName =  nameObject.getAsJsonPrimitive("official");
                JsonPrimitive officialName =  countryElement.getAsJsonObject()
                        .getAsJsonObject("name")
                        .getAsJsonPrimitive("official");
                System.out.println(officialName);
            }

        } catch (IOException e) {
            System.out.println("Server request failed");
        }
    }
}

//read the currency code from the user
//send the request
//get the response
//extract and print the country name associated with the currency code

//List<Country>
//Country
