package com.garranto;

import com.google.gson.*;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class CountryApp
{
    public static void main(String[] args) {
        System.out.println("Country App");
        System.out.println("==============");



//        client object for sending request and receiving the response

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        Gson gson = new Gson();

//        request that needs to be send
        Request request = new Request.Builder()
                .url("https://restcountries.com/v3.1/all")
                .method("GET",null)
                .build();

//        sending request and getting the response back using okhttp client

        try {
            Response response = client.newCall(request).execute();

            String countriesJSONArrayString = response.body().string();


//            array of elements
//            array of objects or array primitives or array of arrays

            JsonArray countriesJSONArray = gson.fromJson(countriesJSONArrayString,JsonArray.class);

            System.out.println(countriesJSONArray.size());

            for(JsonElement countryElement:countriesJSONArray){

                JsonObject country = countryElement.getAsJsonObject();
                JsonObject nameObject = country.getAsJsonObject("name");
                JsonPrimitive officialName =  nameObject.getAsJsonPrimitive("official");
                System.out.println(officialName);
            }

        } catch (IOException e) {
            System.out.println("Server request failed");
        }

    }
}



//send a request to country rest
//get the response and parsing the response
//extracting the required fields(name of all the countries)
//display the name of all the countries

//1. Display all the country names
//2. search a country using currency code


//OkHttpClient client = new OkHttpClient().newBuilder()
//  .build();
//MediaType mediaType = MediaType.parse("text/plain");
//RequestBody body = RequestBody.create(mediaType, "");
//Request request = new Request.Builder()
//  .url("https://restcountries.com/v3.1/all")
//  .method("GET", body)
//  .build();
//Response response = client.newCall(request).execute();



//json array represented as string



//gson

//JsonElement
//jsonArray
//JsonObject
//JSonPrimitive


//between string json object